package com.twofivezero.secretsanta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Levani Buzaladze
 *
 */
@SpringBootApplication
public class SecreSantaApp {
	public static void main(String[] args) {
		SpringApplication.run(SecreSantaApp.class, args);
	}
}
