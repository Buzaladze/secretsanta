package com.twofivezero.secretsanta.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.twofivezero.secretsanta.dtos.GameDTO;
import com.twofivezero.secretsanta.models.BaseResult;
import com.twofivezero.secretsanta.models.ObjectResult;
import com.twofivezero.secretsanta.models.createGameParam;
import com.twofivezero.secretsanta.models.getGamesParam;
import com.twofivezero.secretsanta.models.updateGameParam;
import com.twofivezero.secretsanta.services.GameService;

@RestController
@RequestMapping(value = "/api/game", produces = "application/json", consumes = "application/json")
public class GameController {

	@Autowired
	GameService service;

	@RequestMapping(value = "/", method = RequestMethod.POST)
	private BaseResult createGame(@RequestBody createGameParam param) {
		service.createGame(param);
		return new BaseResult();
	}

	@RequestMapping(value = "/{gameId}", method = RequestMethod.PUT)
	private BaseResult updateGame(@PathVariable("gameId") Long gameId, @RequestBody updateGameParam param) {
		return new BaseResult();
	}

	@RequestMapping(value = "/{gameId}", method = RequestMethod.DELETE)
	private BaseResult deleteGame(@PathVariable("gameId") Long gameId) {
		return new BaseResult();
	}

	@RequestMapping(value = "/{gameId}", method = RequestMethod.GET)
	private ObjectResult<GameDTO> getGame(@PathVariable("gameId") Long gameId) {
		return new ObjectResult<GameDTO>(null);
	}

	@RequestMapping(value = "/{gameId}", method = RequestMethod.GET)
	private ObjectResult<GameDTO> getGames(@RequestBody getGamesParam param) {
		return new ObjectResult<GameDTO>(null);
	}

	@RequestMapping(value = "/{gameId}", method = RequestMethod.PUT)
	private BaseResult removeGamePlayers(@PathVariable("gameId") Long gameId, @RequestBody updateGameParam param) {
		return new BaseResult();
	}

	@RequestMapping(value = "/{gameId}", method = RequestMethod.PUT)
	private BaseResult startGame(@PathVariable("gameId") Long gameId) {
		service.startGame(gameId);
		return new BaseResult();
	}

}
