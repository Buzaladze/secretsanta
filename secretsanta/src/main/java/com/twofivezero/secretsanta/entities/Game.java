package com.twofivezero.secretsanta.entities;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.twofivezero.secretsanta.Enums.GameStatus;

/**
 * @author Levani Buzaladze
 *
 */
@Entity
@Table(name = "GAMES")
public class Game {
	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "NAME")
	private String name;

	@Column(name = "CREATE_DATE")
	private LocalDateTime createDate;

	@Column(name = "UPDATE_DATE")
	private LocalDateTime updateDate;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "PLAYER_ID")
	private Player author;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "GAME_PLAYERS", joinColumns = @JoinColumn(name = "GAME_ID"), inverseJoinColumns = @JoinColumn(name = "PLAYER_ID"))
	private List<Player> participants;

	@Column(name = "IS_ACTIVE")
	private boolean active;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "STATUS")
	private GameStatus status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public Player getAuthor() {
		return author;
	}

	public void setAuthor(Player author) {
		this.author = author;
	}

	public List<Player> getParticipants() {
		return participants;
	}

	public void setParticipants(List<Player> participants) {
		this.participants = participants;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public GameStatus getStatus() {
		return status;
	}

	public void setStatus(GameStatus status) {
		this.status = status;
	}

}
