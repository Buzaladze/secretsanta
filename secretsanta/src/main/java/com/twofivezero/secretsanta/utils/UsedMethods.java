package com.twofivezero.secretsanta.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import com.twofivezero.secretsanta.dtos.CoupleDTO;
import com.twofivezero.secretsanta.dtos.PlayerDTO;

/**
 * @author Levani Buzaladze
 *
 */
public class UsedMethods {
	public static List<CoupleDTO> setCouples(List<PlayerDTO> players) {
		List<PlayerDTO> santas = new ArrayList<>();
		List<PlayerDTO> santaviks = new ArrayList<>();
		santas.addAll(players);
		santaviks.addAll(players);

		List<CoupleDTO> result = new ArrayList<>();

		for (int i = 0; i < players.size(); i++) {
			Random rand = new Random();
			int santaIndex;
			int santavikIndex;
			CoupleDTO dto = new CoupleDTO();
			do {
				santaIndex = rand.nextInt(santas.size());
				santavikIndex = rand.nextInt(santaviks.size());

				dto.setSanta(santas.get(santaIndex));
				dto.setSantavik(santaviks.get(santavikIndex));
			} while (Objects.equals(santas.get(santaIndex), santaviks.get(santavikIndex)));
			result.add(dto);
			santas.remove(santaIndex);
			santaviks.remove(santavikIndex);
		}

		return result;
	}
}
