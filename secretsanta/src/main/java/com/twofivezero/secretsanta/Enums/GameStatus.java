package com.twofivezero.secretsanta.Enums;

public enum GameStatus {
	CREATED(0), STARTED(1), CANCELED(2);
	private int code;

	private GameStatus(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}
}
