package com.twofivezero.secretsanta.Enums;

public enum ErrorCode {
	/**
	 * everything is OK
	 */
	OK(1),
	/**
	 * error is undefined
	 */
	UNDEFINED_ERROR(-1),

	/**
	 * authenticate is required
	 */
	AUTH_ERROR(-100),
	/**
	 * authenticate token is expired or invalid
	 */
	AUTH_INCORRECT_TOKEN_ERROR(-101),
	/**
	 * permission error
	 */
	PERMISSION_ERROR(-102),
	/**
	 * parameters error
	 */
	PARAMETERS_ERROR(-200),
	/**
	 * some parameters are missing or incorrect
	 */
	PARAMTERS_ERROR(-201),
	/**
	 * password strength is not enough
	 */
	PASSWORD_STRENGTH_ERROR(-202),
	/**
	 * account already registered
	 */
	ALREADY_REGISTERED_ERROR(-203);

	private int code;

	private ErrorCode(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}
}
