package com.twofivezero.secretsanta.exceptions;

import org.springframework.transaction.TransactionException;

import com.twofivezero.secretsanta.Enums.ErrorCode;

/**
 * @author Levani Buzaladze
 *
 */
public class BaseException extends TransactionException {
	private static final long serialVersionUID = 6834803026582998840L;

	protected ErrorCode code;

	public BaseException(ErrorCode code) {
		this(code, null);
	}

	public BaseException(ErrorCode code, String msg) {
		super(msg);
		this.code = code;
	}

	public ErrorCode getCode() {
		return code;
	}

	public void setCode(ErrorCode code) {
		this.code = code;
	}
}
