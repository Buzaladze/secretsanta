package com.twofivezero.secretsanta.exceptions;

import com.twofivezero.secretsanta.Enums.ErrorCode;

/**
 * @author Levani Buzaladze
 *
 */
public class ParametersException extends BaseException {
	private static final long serialVersionUID = 8608466811344361262L;

	public ParametersException() {
		super(ErrorCode.PARAMETERS_ERROR);
	}

	public ParametersException(String msg) {
		super(ErrorCode.PARAMETERS_ERROR, msg);
	}
}
