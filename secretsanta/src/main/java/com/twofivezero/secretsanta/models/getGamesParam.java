package com.twofivezero.secretsanta.models;

/**
 * @author Levani Buzaladze
 *
 */
public class getGamesParam extends PagingParam {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "getGamesParam [name=" + name + "]";
	}

}
