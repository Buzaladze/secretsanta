package com.twofivezero.secretsanta.models;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

/**
 * @author Levani Buzaladze
 *
 */
public class PeriodParam {
	@NotNull
	protected LocalDateTime from;

	@NotNull
	protected LocalDateTime to;

	public LocalDateTime getFrom() {
		return from;
	}

	public void setFrom(LocalDateTime from) {
		this.from = from;
	}

	public LocalDateTime getTo() {
		return to;
	}

	public void setTo(LocalDateTime to) {
		this.to = to;
	}

	@Override
	public String toString() {
		return "PeriodParam [from=" + from + ", to=" + to + "]";
	}
}
