package com.twofivezero.secretsanta.models;

import com.twofivezero.secretsanta.Enums.ErrorCode;

/**
 * @author Levani Buzaladze
 *
 */
public class BaseResult {
	protected int resultCode = ErrorCode.OK.getCode();

	protected String resultMsg;

	public BaseResult() {
	}

	public BaseResult(int resultCode) {
		this.resultCode = resultCode;
	}

	public BaseResult(int resultCode, String resultMsg) {
		this.resultCode = resultCode;
		this.resultMsg = resultMsg;
	}

	public int getResultCode() {
		return resultCode;
	}

	public void setResultCode(int resultCode) {
		this.resultCode = resultCode;
	}

	public String getResultMsg() {
		return resultMsg;
	}

	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}

	@Override
	public String toString() {
		return "BaseResult [resultCode=" + resultCode + ", resultMsg=" + resultMsg + "]";
	}
}
