package com.twofivezero.secretsanta.models;

/**
 * @author Levani Buzaladze
 *
 */
public class ObjectResult<T> extends BaseResult {
	private T result;

	public ObjectResult(T result) {
		super();
		this.result = result;
	}

	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "ObjectResult [result=" + result + ", resultCode=" + resultCode + ", resultMsg=" + resultMsg + "]";
	}

}