package com.twofivezero.secretsanta.models;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * @author Levani Buzaladze
 *
 */
public class PagingParam extends PeriodParam {
	protected int offset;
	@Min(1)
	@Max(100)
	protected int limit;

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	@Override
	public String toString() {
		return "PagingParam [offset=" + offset + ", limit=" + limit + "]";
	}
}
