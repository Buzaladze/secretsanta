package com.twofivezero.secretsanta.models;

import java.util.List;

import com.twofivezero.secretsanta.dtos.PlayerDTO;

/**
 * @author Levani Buzaladze
 *
 */
public class updateGameParam {
	private String name;

	private List<PlayerDTO> players;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<PlayerDTO> getPlayers() {
		return players;
	}

	public void setPlayers(List<PlayerDTO> players) {
		this.players = players;
	}

	@Override
	public String toString() {
		return "updateGameParam [name=" + name + ", players=" + players + "]";
	}

}
