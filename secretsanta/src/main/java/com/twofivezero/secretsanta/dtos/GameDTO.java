package com.twofivezero.secretsanta.dtos;

import java.time.LocalDateTime;
import java.util.List;

import com.twofivezero.secretsanta.entities.Player;

public class GameDTO {
	private Long id;

	private String name;

	private LocalDateTime createDate;

	private LocalDateTime updateDate;

	private PlayerDTO author;

	private List<Player> participants;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public PlayerDTO getAuthor() {
		return author;
	}

	public void setAuthor(PlayerDTO author) {
		this.author = author;
	}

	public List<Player> getParticipants() {
		return participants;
	}

	public void setParticipants(List<Player> participants) {
		this.participants = participants;
	}

}
