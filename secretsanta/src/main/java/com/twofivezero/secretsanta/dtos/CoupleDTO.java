package com.twofivezero.secretsanta.dtos;

/**
 * @author Levani Buzaladze
 *
 */
public class CoupleDTO {
	private PlayerDTO santa;

	private PlayerDTO santavik;

	public PlayerDTO getSanta() {
		return santa;
	}

	public void setSanta(PlayerDTO santa) {
		this.santa = santa;
	}

	public PlayerDTO getSantavik() {
		return santavik;
	}

	public void setSantavik(PlayerDTO santavik) {
		this.santavik = santavik;
	}

	@Override
	public String toString() {
		return "CoupleDTO [santa=" + santa + ", santavik=" + santavik + "]";
	}

}
