package com.twofivezero.secretsanta.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.twofivezero.secretsanta.Enums.GameStatus;
import com.twofivezero.secretsanta.entities.Game;
import com.twofivezero.secretsanta.entities.Player;
import com.twofivezero.secretsanta.exceptions.ParametersException;
import com.twofivezero.secretsanta.models.createGameParam;

@Service
public class GameService {

	@Autowired
	DbService dbService;

	public void createGame(createGameParam param) {

		Long loggedUserId = 0L;
		Player author = dbService.getPlayerById(loggedUserId);

		Game game = new Game();
		game.setName(param.getName());
		game.setAuthor(author);
		game.setStatus(GameStatus.CREATED);

		dbService.createGame(game);
	}

	public void startGame(Long gameId) {
		Game game = dbService.getGameById(gameId);
		if (game == null)
			throw new ParametersException("Game :" + gameId + " not found ");
		game.setStatus(GameStatus.STARTED);
		
		dbService.updateGame(game);
	}

}
