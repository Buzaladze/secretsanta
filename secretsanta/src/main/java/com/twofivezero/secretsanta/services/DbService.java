package com.twofivezero.secretsanta.services;

import java.time.LocalDateTime;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.twofivezero.secretsanta.entities.Game;
import com.twofivezero.secretsanta.entities.Player;

@Service
public class DbService {

	@Autowired
	EntityManager em;

	public Player getPlayerById(Long loggedUserId) {
		// TODO Auto-generated method stub
		return null;
	}

	public void createGame(Game game) {
		game.setCreateDate(LocalDateTime.now());
		game.setUpdateDate(LocalDateTime.now());
		em.persist(game);
	}

	public Game getGameById(Long gameId) {
		Query query = em.createQuery("from Game g where g.id = :gameId and g.active is true");

		query.setParameter("gameId", gameId);
		return (Game) query.getSingleResult();
	}

	public void updateGame(Game game) {
		game.setUpdateDate(LocalDateTime.now());
		em.persist(game);
	}

}
